##
# Gradient Descent Algorithm for a Linear Regression
# problem where the Linear Regression Model is h(x) = theta0 + theta1x
##


import math

# each row is (xi and yi)
training_set = [
	[3, 2],
	[1, 2],
	[0, 1],
	[4, 3]
]

thetas = (0.0, 0.0)

def calculate_squared_error(training_set, coefficients):
	error = 0.0
	for entry in training_set:
		sum_err = coefficients[0] + coefficients[1] * entry[0] - entry[1]
		error += sum_err * sum_err
	return error/(2 * len(training_set))

def calculate_partial_derivative_theta0(training_set, coefficients):
	value = 0.0
	for entry in training_set:
		value += (coefficients[0] + coefficients[1] * entry[0] - entry[1])
	return value

def calculate_partial_derivative_theta1(training_set, coefficients):
	value = 0.0
	for entry in training_set:
		value += entry[0] * (coefficients[0] + coefficients[1] * entry[0] - entry[1])
	return value

def gradient_descent(training_set, coefficients, alpha):
	at_minimum = False
	theta0, theta1 = coefficients[0], coefficients[1]
	temp0, temp1 = theta0, theta1
	while(not at_minimum):
		## for all possible steps that you can take, figure out which one reduces the value of the cost function
		temp0 = theta0 - (alpha * (1.0/len(training_set)) * calculate_partial_derivative_theta0(training_set, (theta0, theta1)))
		temp1 = theta1 - (alpha * (1.0/len(training_set)) * calculate_partial_derivative_theta1(training_set, (theta0, theta1)))
		# print((theta0 - (alpha * calculate_partial_derivative_theta0(training_set, (theta0, theta1)))) - theta0 == 0)
		if (math.fabs(temp0 - theta0) == 0 and math.fabs(temp1 - theta1) == 0):
			at_minimum = True
		else:
			theta0 = temp0
			theta1 = temp1
	return (theta0, theta1)

thetas = gradient_descent(training_set, thetas, 0.01)

print('The regression model with minimum error is: ', thetas[0], ' + ', thetas[1], 'x')
